#!/bin/sh -e

VERSION=$2
TAR=../sisu-ioc_$VERSION.orig.tar.xz
DIR=sisu-ioc-$VERSION

mkdir $DIR
tar xzf $3 --strip-components=1 -C $DIR
XZ_OPT=--best tar cJf $TAR -X debian/orig-tar.exclude $DIR
rm -rf $DIR
